export class LocationDistance {
  distanceInKm: number;
  lat: number;
  lon: number;
}
