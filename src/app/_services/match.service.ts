import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';


import { Match } from '../_models/Match';
import { environment } from 'src/environments/environment';
import { FilterCriteria } from '../_models/FilterCriteria';

@Injectable({
  providedIn: 'root'
})
export class MatchService {

  constructor(private http: HttpClient) { }
  getMatchById(id: number): Observable<Match> {
    return this.http.get<Match>(`${environment.matchApi}/${id}`);
  }

  getMatches(filterCriteria: FilterCriteria): Observable<Match[]>  {
    return this.http.post<Match[]>(`${environment.matchApi}/filter`, filterCriteria);
  }

  alterMatches(match: Match): Observable<Match> {
    return this.http.post<Match>(`${environment.matchApi}/update`, match);
  }

}

